package br.com.veiculos.model;
import java.util.*;

public class Oficina {
	Random r = new Random();

	public Veiculo proximo() {
		Veiculo v;
		int code = r.nextInt();
		if (code % 2 == 0)
			v = new Automovel();
		else
			v = new Bicicleta();
		return v;
	}

	public void manutencao(Veiculo vi) {
		vi.listaVerificacoes();
		vi.ajustar();
		vi.limpar();
		if (vi instanceof Automovel)
			((Automovel) vi).mudarOleo();
	}
}