package br.com.veiculos.dao;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.veiculos.bean.Automovel;
import br.com.veiculos.bean.Bicicleta;
import br.com.veiculos.bean.Veiculo;

public class Oficina {
	
	Random r = new Random();

	public Veiculo proximo() {
		Veiculo v;
		int code = r.nextInt();
		if (code % 2 == 0)
			v = new Automovel();
		else
			v = new Bicicleta();
		return v;
	}

	public void manutencao(Veiculo vi) {
		vi.listaVerificacoes();
		vi.ajustar();
		vi.limpar();
		if (vi instanceof Automovel)
			((Automovel) vi).mudarOleo();
	}
	
}