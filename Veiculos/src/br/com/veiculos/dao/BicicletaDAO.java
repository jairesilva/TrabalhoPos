package br.com.veiculos.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.veiculos.bean.Bicicleta;

public class BicicletaDAO {

	private static BicicletaDAO instance;
	protected EntityManager entityManager;

	public static BicicletaDAO getInstance() {
		if (instance == null) {
			instance = new BicicletaDAO();
		}

		return instance;
	}

	private BicicletaDAO() {
	                   entityManager = getEntityManager();
	         }

	private EntityManager getEntityManager() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("trabalho_pos");
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}

		return entityManager;
	}

	public Bicicleta getById(final int id) {
		return entityManager.find(Bicicleta.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Bicicleta> findAll() {
		return entityManager.createQuery("FROM " + Bicicleta.class.getName()).getResultList();
	}

	public void persist(Bicicleta Bicicleta) {
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(Bicicleta);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void merge(Bicicleta Bicicleta) {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(Bicicleta);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void remove(Bicicleta Bicicleta) {
		try {
			entityManager.getTransaction().begin();
			Bicicleta = entityManager.find(Bicicleta.class, Bicicleta.getId());
			entityManager.remove(Bicicleta);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void removeById(final int id) {
		try {
			Bicicleta Bicicleta = getById(id);
			remove(Bicicleta);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
